package arquivoDeDevolucao

type Produtos struct {
	Fixo               string `json:"Fixo"`
	CodigoEan          string `json:"CodigoEan"`
	LoteItem           string `json:"LoteItem"`
	QuantidadeItem     string `json:"QuantidadeItem"`
	ValorTotalProduto  string `json:"ValorTotalProduto"`
	MotivoCancelamento string `json:"MotivoCancelamento"`
}
