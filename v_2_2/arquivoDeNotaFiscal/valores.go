package arquivoDeNotaFiscal

type Valores struct {
	Fixo               string `json:"Fixo"`
	ValorNota          string `json:"ValorNota"`
	ValorDescontoNota  string `json:"ValorDescontoNota"`
	ValorRepasseICMS   string `json:"ValorRepasseICMS"`
	ValorTotalProdutos string `json:"ValorTotalProdutos"`
	ValorIPI           string `json:"ValorIPI"`
	ValorFrete         string `json:"ValorFrete"`
	ValorSeguro        string `json:"ValorSeguro"`
}
