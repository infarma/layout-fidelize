package arquivoDeNotaFiscal

type Produtos struct {
	Fixo                                  string `json:"Fixo"`
	CodigoEan                             string `json:"CodigoEan"`
	QuantidadeAtendida                    string `json:"QuantidadeAtendida"`
	PercentualDescontoProduto             string `json:"PercentualDescontoProduto"`
	ValorUnitarioDesconto                 string `json:"ValorUnitarioDesconto"`
	ValorUnitarioLiquidoProduto           string `json:"ValorUnitarioLiquidoProduto"`
	BaseCalculoICMS                       string `json:"BaseCalculoICMS"`
	BaseCalculoICMSSubstituicaoTributaria string `json:"BaseCalculoICMSSubstituicaoTributaria"`
	PercentualICMS                        string `json:"PercentualICMS"`
	PercentualIPI                         string `json:"PercentualIPI"`
	ValorICMS                             string `json:"ValorICMS"`
	ValorST                               string `json:"ValorST"`
	ValorICMSRepassado                    string `json:"ValorICMSRepassado"`
	CFOP                                  string `json:"CFOP"`
	FlagSubstituicaoTributaria            string `json:"FlagSubstituicaoTributaria"`
	IdentificadorListaPositiva            string `json:"IdentificadorListaPositiva"`
	PrazoProduto                          string `json:"PrazoProduto"`
	ClassificacaoProduto                  string `json:"ClassificacaoProduto"`
	DCB                                   string `json:"DCB"`
	ValorRepasse                          string `json:"ValorRepasse"`
	ValorTotalProduto                     string `json:"ValorTotalProduto"`
	ValorTotalProdutoMaisEncargos         string `json:"ValorTotalProdutoMaisEncargos"`
	CodigoSituacaoTributaria              string `json:"CodigoSituacaoTributaria"`
	PercentualDescontoFinanceiro          string `json:"PercentualDescontoFinanceiro"`
	ValorDescontoFinanceiro               string `json:"ValorDescontoFinanceiro"`
	TipoProduto                           string `json:"TipoProduto"`
}
