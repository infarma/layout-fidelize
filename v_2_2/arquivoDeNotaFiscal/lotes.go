package arquivoDeNotaFiscal

type Lotes struct {
	Fixo                    string `json:"Fixo"`
	EanProduto              string `json:"EanProduto"`
	Lote                    string `json:"Lote"`
	QuantidadeReferenteLote string `json:"QuantidadeReferenteLote"`
	Vencimento              string `json:"Vencimento"`
}
