package arquivoDePedido

import "strings"

type IdentificacaoPedido struct {
	Fixo                       string	`json:"Fixo"`
	CodigoEan                  string	`json:"CodigoEan"`
	QuantidadePedida           string	`json:"QuantidadePedida"`
	PercentualUnitarioDesconto string	`json:"PercentualUnitarioDesconto"`
	ValorUnitarioLiquido       string	`json:"ValorUnitarioLiquido"`
	PrazoProdutos              string	`json:"PrazoProdutos"`
	FlagProdutoMonitorado      string	`json:"FlagProdutoMonitorado"`
}

func (i *IdentificacaoPedido) ComposeStruct(fileContents string) {
	entradas := strings.Split(fileContents, ";")

	i.Fixo = entradas[16]
	i.CodigoEan = entradas[17]
	i.QuantidadePedida = entradas[18]
	i.PercentualUnitarioDesconto = entradas[19]
	i.ValorUnitarioLiquido = entradas[20]
	i.PrazoProdutos = entradas[21]
	i.FlagProdutoMonitorado = entradas[22]
}