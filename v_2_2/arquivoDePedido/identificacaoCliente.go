package arquivoDePedido

import "strings"

type IdentificacaoCliente struct {
	Fixo                  string `json:"Fixo"`
	CnpjCpfCliente        string `json:"CnpjCpfCliente"`
	Email                 string `json:"Email"`
	CnpjDistribuidor      string `json:"CnpjDistribuidor"`
	PrazoNegociado        string `json:"PrazoNegociado"`
	TipoVenda             string `json:"TipoVenda"`
	CodigoPedidoCliente   string `json:"CodigoPedidoCliente"`
	CodigoPedido          string `json:"CodigoPedido"`
	Margem                string `json:"Margem"`
	FlagTipoPedido        string `json:"FlagTipoPedido"`
	VersaoLayout          string `json:"VersaoLayout"`
	SiglaIndustria        string `json:"SiglaIndustria"`
	FlagRecalculoDesconto string `json:"FlagRecalculoDesconto"`
	FlagCnpjCpf           string `json:"FlagCnpjCpf"`
	ApontadorComercial    string `json:"ApontadorComercial"`
	CodigoTeleoperador    string `json:"CodigoTeleoperador"`
}

func (i *IdentificacaoCliente) ComposeStruct(fileContents string) {
	entradas := strings.Split(fileContents, ";")

	i.Fixo = entradas[0]
	i.CnpjCpfCliente = entradas[1]
	i.Email = entradas[2]
	i.CnpjDistribuidor = entradas[3]
	i.PrazoNegociado = entradas[4]
	i.TipoVenda = entradas[5]
	i.CodigoPedidoCliente = entradas[6]
	i.CodigoPedido = entradas[7]
	i.Margem = entradas[8]
	i.FlagTipoPedido = entradas[9]
	i.VersaoLayout = entradas[10]
	i.SiglaIndustria = entradas[11]
	i.FlagRecalculoDesconto = entradas[12]
	i.FlagCnpjCpf = entradas[13]
	i.ApontadorComercial = entradas[14]
	i.CodigoTeleoperador = entradas[15]
}