package arquivoDePedido

import "strings"

type FinalizadorPedido struct {
	Fixo          string	`json:"Fixo"`
	TotalProdutos string	`json:"TotalProdutos"`
}

func (i *FinalizadorPedido) ComposeStruct(fileContents string) {
	entradas := strings.Split(fileContents, ";")

	i.Fixo = entradas[17]
	i.TotalProdutos = entradas[18]
}