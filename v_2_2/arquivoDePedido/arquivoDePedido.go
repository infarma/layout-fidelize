package arquivoDePedido

import (
	"bufio"
	"os"
)

type ArquivoDePedido struct {
	IdentificacaoCliente IdentificacaoCliente `json:"IdentificacaoCliente"`
	IdentificacaoPedido  IdentificacaoPedido  `json:"IdentificacaoPedido"`
	FinalizadorPedido    FinalizadorPedido    `json:"FinalizadorPedido"`
}

func GetStruct(fileHandle *os.File) (ArquivoDePedido, error) {
	fileScanner := bufio.NewScanner(fileHandle)
	arquivo := ArquivoDePedido{}
	var err error

	conteudoArquivo := fileScanner.Text()
	arquivo.IdentificacaoCliente.ComposeStruct(conteudoArquivo)
	arquivo.IdentificacaoPedido.ComposeStruct(conteudoArquivo)
	arquivo.FinalizadorPedido.ComposeStruct(conteudoArquivo)

	return arquivo, err
}
