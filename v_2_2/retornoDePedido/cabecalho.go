package retornoDePedido

type Cabecalho struct {
	Fixo                   string `json:"Fixo"`
	CodigoPedidoFornecedor string `json:"CodigoPedidoFornecedor"`
	DataHoraProcessamento  string `json:"DataHoraProcessamento"`
	Filler                 string `json:"Filler"`
	PrazoFaturado          string `json:"PrazoFaturado"`
	MotivoPedido           string `json:"MotivoPedido"`
	FlagTipoPedido         string `json:"FlagTipoPedido"`
	VersaoLayout           string `json:"VersaoLayout"`
	SiglaIndustria         string `json:"SiglaIndustria"`
}
