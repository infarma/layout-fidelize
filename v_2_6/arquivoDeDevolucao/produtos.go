package arquivoDeDevolucao

type Produtos struct {
	Fixo                    string `json:"Fixo"`
	CodigoEan               string `json:"CodigoEan"`
	LoteItem                string `json:"LoteItem"`
	QuantidadeItemDevolvida string `json:"QuantidadeItemDevolvida"`
	ValorTotalProduto       string `json:"ValorTotalProduto"`
	MotivoCancelamento      string `json:"MotivoCancelamento"`
}
