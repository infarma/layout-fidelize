package arquivoDeDevolucao

type Finalizador struct {
	Fixo                 string `json:"Fixo"`
	TotalItensNota       string `json:"TotalItensNota"`
	TotalItensDevolvidos string `json:"TotalItensDevolvidos"`
}
