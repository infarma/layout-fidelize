package arquivoDeDevolucao

type Cabecalho struct {
	Fixo               string `json:"Fixo"`
	DataGeracaoArquivo string `json:"DataGeracaoArquivo"`
	CnpjCD             string `json:"CnpjCD"`
	NumeroNotaFiscal   string `json:"NumeroNotaFiscal"`
	CodigoPedido       string `json:"CodigoPedido"`
	Tipo               string `json:"Tipo"`
}
