package arquivoDePedido

type IdentificacaoPedido struct {
	Fixo                       string `json:"Fixo"`
	CodigoEan                  string `json:"CodigoEan"`
	QuantidadePedida           string `json:"QuantidadePedida"`
	PercentualUnitarioDesconto string `json:"PercentualUnitarioDesconto"`
	ValorUnitarioLiquido       string `json:"ValorUnitarioLiquido"`
	PrazoProdutos              string `json:"PrazoProdutos"`
	FlagProdutoMonitorado      string `json:"FlagProdutoMonitorado"`
	CodigoProdutoIndustria     string `json:"CodigoProdutoIndustria"`
	PrecoBruto                 string `json:"PrecoBruto"`
}
