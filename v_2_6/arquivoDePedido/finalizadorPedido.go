package arquivoDePedido

type FinalizadorPedido struct {
	Fixo          string	`json:"Fixo"`
	TotalProdutos string	`json:"TotalProdutos"`
}