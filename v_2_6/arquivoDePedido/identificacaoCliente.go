package arquivoDePedido

type IdentificacaoCliente struct {
	Fixo                  string `json:"Fixo"`
	CnpjCpfCliente        string `json:"CnpjCpfCliente"`
	Email                 string `json:"Email"`
	CnpjDistribuidor      string `json:"CnpjDistribuidor"`
	PrazoNegociado        string `json:"PrazoNegociado"`
	TipoVenda             string `json:"TipoVenda"`
	CodigoPedidoCliente   string `json:"CodigoPedidoCliente"`
	CodigoPedido          string `json:"CodigoPedido"`
	Margem                string `json:"Margem"`
	FlagTipoPedido        string `json:"FlagTipoPedido"`
	VersaoLayout          string `json:"VersaoLayout"`
	SiglaIndustria        string `json:"SiglaIndustria"`
	FlagRecalculoDesconto string `json:"FlagRecalculoDesconto"`
	FlagCnpjCpf           string `json:"FlagCnpjCpf"`
	ApontadorComercial    string `json:"ApontadorComercial"`
	CodigoTeleoperador    string `json:"CodigoTeleoperador"`
	CodigoCliente         string `json:"CodigoCliente"`
}
