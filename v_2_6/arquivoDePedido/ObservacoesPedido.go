package arquivoDePedido

type ObservacoesPedido struct {
	Fixo       string `json:"Fixo"`
	Observacao string `json:"Observacao"`
}
