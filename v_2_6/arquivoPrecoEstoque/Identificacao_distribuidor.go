package arquivoPrecoEstoque

type IdentificacaoDistribuidor struct {
	Fixo             string `json:"Fixo"`
	CnpjDistribuidor string `json:"CnpjDistribuidor"`
}
