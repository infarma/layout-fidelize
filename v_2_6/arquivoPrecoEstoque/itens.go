package arquivoPrecoEstoque

type Itens struct {
	Fixo                    string `json:"Fixo"`
	CodigoEanProduto        string `json:"CodigoEanProduto"`
	PrecoProdutoCD          string `json:"PrecoProdutoCD"`
	EstoqueCD               string `json:"EstoqueCD"`
	EstoqueEmTransitoParaCD string `json:"EstoqueEmTransitoParaCD"`
	PrazoProduto            string `json:"PrazoProduto"`
	UF            string `json:"UF"`
}
