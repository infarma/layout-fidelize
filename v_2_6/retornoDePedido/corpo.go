package retornoDePedido

type Corpo struct {
	Fixo                              string `json:"Fixo"`
	CodigoEan                         string `json:"CodigoEan"`
	QuantidadeAtendida                string `json:"QuantidadeAtendida"`
	PercentualUnitarioDescontoProduto string `json:"PercentualUnitarioDescontoProduto"`
	ValorUnitarioDesconto             string `json:"ValorUnitarioDesconto"`
	ValorUnitarioLiquidoProduto       string `json:"ValorUnitarioLiquidoProduto"`
	Motivo                            string `json:"Motivo"`
	EanFaturado                       string `json:"EanFaturado"`
	FlagProdutoMonitorado             string `json:"FlagProdutoMonitorado"`
	MotivoDistribuidor                string `json:"MotivoDistribuidor"`
}
