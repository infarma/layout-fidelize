package arquivoDeNotaFiscal

type Cabecalho struct {
	Fixo                   string `json:"Fixo"`
	CodigoPedidoFornecedor string `json:"CodigoPedidoFornecedor"`
	DataProcessamento      string `json:"DataProcessamento"`
	HoraProcessamento      string `json:"HoraProcessamento"`
	DataEmissaoNota        string `json:"DataEmissaoNota"`
	CnpjFornecedor         string `json:"CnpjFornecedor"`
	SiglaIndustria         string `json:"SiglaIndustria"`
	CnpjFaturado           string `json:"CnpjFaturado"`
}
