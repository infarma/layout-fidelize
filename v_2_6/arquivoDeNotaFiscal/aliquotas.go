package arquivoDeNotaFiscal

type Aliquotas struct {
	Fixo            string `json:"Fixo"`
	AliquotasICMS   string `json:"AliquotasICMS"`
	BaseCalculoICMS string `json:"BaseCalculoICMS"`
	ValorTotalICMS  string `json:"ValorTotalICMS"`
}
