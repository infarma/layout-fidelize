package arquivoDeNotaFiscal

type ContinuacaoCabecalho struct {
	Fixo                              string `json:"Fixo"`
	NumeroNota                        string `json:"NumeroNota"`
	BaseCalculoSubstituicaoTributaria string `json:"BaseCalculoSubstituicaoTributaria"`
	BaseCalculoICMS                   string `json:"BaseCalculoICMS"`
	ModeloNotaFiscal                  string `json:"ModeloNotaFiscal"`
	SerieNotaFiscal                   string `json:"SerieNotaFiscal"`
	ChaveDANFe                        string `json:"ChaveDANFe"`
}
