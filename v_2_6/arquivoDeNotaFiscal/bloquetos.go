package arquivoDeNotaFiscal

type Bloquetos struct {
	Fixo                         string `json:"Fixo"`
	NumeroBloqueto               string `json:"NumeroBloqueto"`
	DataVencimento               string `json:"DataVencimento"`
	ValorBloqueto                string `json:"ValorBloqueto"`
	DataEmissao                  string `json:"DataEmissao"`
	ValorParcela                 string `json:"ValorParcela"`
	DescontoPorAntecipacao       string `json:"DescontoPorAntecipacao"`
	JuroPorDia                   string `json:"JuroPorDia"`
	DescontoFinanceiro           string `json:"DescontoFinanceiro"`
	Multa                        string `json:"Multa"`
	CodigoBanco                  string `json:"CodigoBanco"`
	CodigoAgencia                string `json:"CodigoAgencia"`
	NumeroContaCorrente          string `json:"NumeroContaCorrente"`
	ValorTotalDescontoFinanceiro string `json:"ValorTotalDescontoFinanceiro"`
}
