package arquivoDeNotaFiscal

type Finalizador struct {
	Fixo                    string `json:"Fixo"`
	QuantidadeLinhasArquivo string `json:"QuantidadeLinhasArquivo"`
}
