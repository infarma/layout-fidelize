package arquivoDeNotaFiscal

type FinalizadorNota struct {
	Fixo                 string `json:"Fixo"`
	ValorTotalICMS       string `json:"ValorTotalICMS"`
	ValorTotalICMSRetido string `json:"ValorTotalICMSRetido"`
	QuantidadeVolume     string `json:"QuantidadeVolume"`
}
