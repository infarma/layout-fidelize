package layout_fidelize

import (
	arquivoDePedido22 "bitbucket.org/infarma/layout-fidelize/v_2_2/arquivoDePedido"
	"os"
)

func GetArquivoDePedido22(fileHandle *os.File) (arquivoDePedido22.ArquivoDePedido, error) {
	return arquivoDePedido22.GetStruct(fileHandle)
}
